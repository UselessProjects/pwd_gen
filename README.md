# 密語產生器
## 規則
```
輸入一個包含大小寫的句子，例如 HelloEveryOne
會將所有大寫擺於前：HEOelloveryne
再刪重複的字母（區分大小寫）：HEOelovryn
```

## 要求
```
密碼產生器 OwO
版本：v1.0.0
作者：pan93412

用法：python3 (Windows: python) pwdgen.py [原文] ([-h]|[--help])

[原文]: 包含大小寫的句子，例如：HelloEveryOne
       <非必須>

[-h] [--help]:  顯示說明訊息
若不指定原文，開啟程式則會從 STDIN 標準輸入讀取原文。
將會把輸出結果輸出於 STDOUT 標準輸出。
```

- 要有說明文字 (10%)
- 允許從標準輸入讀取原文（可使用測試工具：`bash UnitTest.sh (程式檔名) --stdin`）(45%)
- 允許使用第二個參數讀取原文（可使用測試工具：`bash UnitTest.sh (程式檔名)`）(45%)

## 目前實作語言
- C++
  - C++ 實作 1（作者：Lamhaoyin）
  - C++ 實作 2（作者：mingtsay）[1, 45%]
- Python
  - Python 實作 1（作者：pan93412）
  - Python 實作 2（作者：@nekomata_saren）
- Golang（作者：pan93412）
- JavaScript（作者：gnehs）[1]
- Haskell（作者：koru1130）（未測試）[1, 45%]
- PHP（作者：mingtsay）[1, 45%]

[1] 實作不完整的程式
