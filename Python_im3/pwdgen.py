#!/usr/bin/python3

'''
密碼產生器 OwO

輸入一個包含大小寫的句子，例如 HelloEveryOne
會將所有大寫擺於前：HEOelloveryne
再刪重複的字母（區分大小寫）：HEOelovryn

版本：v1.0.0
'''

import sys # 用於載入參數

# CONSTS
author = "pan93412"
version = "v1.0.0"

# PRE-DEFINED VARIABLES
o_txt = "" # original_text

# USAGE TXT
usage = f"""密碼產生器 OwO [實作 3]
版本：{version}
作者：{author}

用法：python3 (Windows: python) {__file__} [原文] ([-h]|[--help])

[原文]: 包含大小寫的句子，例如：HelloEveryOne
       <非必須>

[-h] [--help]:  顯示說明訊息
若不指定原文，開啟程式則會從 STDIN 標準輸入讀取原文。
將會把輸出結果輸出於 STDOUT 標準輸出。
"""

# Args Determine
if "-h" in sys.argv or "--help" in sys.argv:
    print(usage)
    exit(1)
elif len(sys.argv) == 2:
    o_txt = sys.argv[1]
else:
    o_txt = input()
    
# Start convert.
def converter(original_text):
    '''
    輸入一個包含大小寫的句子 (original_text)，例如 HelloEveryOne
    會將所有大寫擺於前：HEOelloveryne
    再刪重複的字母（區分大小寫）：HEOelovryn
    '''
    
    # Step 1
    Step1 = ""
    Step1_Lowers = ""
    for char in original_text:
        if char == char.upper():
            Step1 += char
        if char == char.lower():
            Step1_Lowers += char
    Step1 += Step1_Lowers # Merge
    
    # Step 2
    result = ""
    for char in Step1:
        if char not in result:
            result += char
        else:
            continue

    # Return result.
    return result

# RUN!
print(converter(o_txt))
exit()
