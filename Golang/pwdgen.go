// 密碼產生器 OwO
// 
// 輸入一個包含大小寫的句子，例如 HelloEveryOne
//
// 再將所有大寫擺於前：HEOelloveryne
//
// 再刪重複的字母（區分大小寫）：HEOelovryn
// 
// 版本：v1.0.0；
// 作者：pan93412；
// 移植自 Python 版

package main

import (
    "os"
    "fmt"
    "bufio" // just for input()... Orz
    "strings"
)

/* Consts */
const author, version = "pan93412", "v1.0.0"
var args = os.Args
var usage = fmt.Sprintf(`密碼產生器 OwO
版本：%s
作者：%s

用法：%s [原文] ([-h]|[--help])

[原文]: 包含大小寫的句子，例如：HelloEveryOne
       <非必須>

[-h] [--help]:  顯示說明訊息
若不指定原文，開啟程式則會從 STDIN 標準輸入讀取原文。
將會把輸出結果輸出於 STDOUT 標準輸出。`, version, author, args[0])

/* Pre-defined variables */
var oText = ""

// Check whether val in args and return the result.
func isvalexist(val string) bool {
    for _, arg := range args {
        if arg == val {
            return true
        }
    }
    return false
}

// Input with prompt.
func input(prompt string) (userInput string) {
    var userInputTmp string
    var stdinReader = bufio.NewReader(os.Stdin)
    var err error
    
    fmt.Print(prompt)
    userInputTmp, err = stdinReader.ReadString('\n')
    
    if err != nil {
        panic(err)
    }
    
    return strings.Replace(userInputTmp, "\n", "", -1)
}

// 輸入一個包含大小寫的句子 (originalTxt)，例如 HelloEveryOne
//
// 再將所有大寫擺於前：HEOelloveryne
//
// 再刪重複的字母（區分大小寫）：HEOelovryn
func converter(originalTxt string) string {
    // Step 1
    var Step1 , Step1_Lower string
    
    for _, char := range originalTxt {
        if string(char) == strings.ToUpper(string(char)) {
            Step1 += string(char)
        } else {
            Step1_Lower += string(char)
        }
    }

    Step1 += Step1_Lower

    // Step 2
    var result string
    
    for _, char := range Step1 {
        if !strings.Contains(result, string(char)) {
            result += string(char)
        } else {
            continue
        }
    }
    
    return result
}

func main() {
    /* Args Determine */
    if isvalexist("-h") || isvalexist("--help") {
        fmt.Println(usage)
        os.Exit(1)
    } else if len(args) == 2 {
        oText = args[1]
    } else {
        oText = input("")
    }
    
    fmt.Println(converter(oText))
    os.Exit(0)
}
