# 單元測試工具
# 用法 `bash UnitTest.sh [程式檔名]`
# 將會呼叫 `[程式檔名] (測試內容)`，所以請確保程式會收第二參數。

function unitTestCheck {
    if [[ ! "$2" == "$1" ]]
    then
        echo "測試 $4 執行失敗：測試 $3，期望 $2，得到 $1"
        exit 1
    fi
}

function unitTest {
    declare -A unit_test
    unit_test=(
        ["1"]="OAOAOAwWwW OAWw"
        ["2"]="ChiNeSEAcOQckmAKOfAK CNSEAOQKhieckmf"
        ["3"]="HelloWorld HWelord"
        ["4"]="Hm Hm"
        ["5"]="hM Mh"
        ["6"]="ISkjudhnjHUAJKLuhnuWYHufuNHSKJhufnjSHUNKgnhufkjHSUNjhygunfjnHNSUAJNZbghfHNSUhfnhdHEHNSUuehNHSJNfnhdUSJKfnjd ISHUAJKLWYNZEkjudhnfgybe"
    )

    echo "共 ${#unit_test[@]} 項單元測試。程序開始！"
    
    for id in ${!unit_test[@]}
    do
        echo "正在進行編號為 $id 的單元測試……"
        test_data=(${unit_test[$id]})
        if [[ $2 == "--stdin" ]]
        then
            unitTestCheck "$(echo ""${test_data[0]}"" | $1 )" ${test_data[1]} ${test_data[0]} ${id}
        else
            unitTestCheck "$($1 ${test_data[0]})" ${test_data[1]} ${test_data[0]} ${id}
        fi
    done

    echo "程式測試通過！ :)"
    exit 0
}

if [[ "$1" == "" ]]
then
	echo "Usage: bash UnitTest.sh FILENAME [--stdin]"
	echo "[--stdin]: 傳送測資到 stdin。"
else
	unitTest $1 $2
fi
