#include <bits/stdc++.h>
using namespace std;

string str, b, s;
bool vis[128];

int main(int argc, char **argv)
{
	for (int i = 0; i < argc; ++i)
		if ((!strcmp(argv[i], "-h")) || (!strcmp(argv[i], "--help")))
		{
			printf("密碼產生器 OwO\n版本：v0.0.1\n作者：Lamhaoyin\n\n用法：%s [原文] ([-h]|[--help])\n\n[原文]: 包含大小寫的句子，例如：HelloEveryOne <非必須>\n[-h] [--help]:  顯示說明訊息\n若不指定原文，開啟程式則會從 STDIN 標準輸入讀取原文。\n將會把輸出結果輸出於 STDOUT 標準輸出。\n", argv[0]);
			exit(1);
		}

	if (argc != 2)
	{
		cin >> str;
	}
	else
		str = argv[1];

	for (auto i : str)
		if (isalpha(i) && !vis[i])
		{
			vis[i] = true;
			if (i == tolower(i))
				s.push_back(i);
			else
				b.push_back(i);
		}

	cout << b << s << endl;

	return 0;
}
