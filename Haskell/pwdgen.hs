import Data.Char
import Data.List

pwdgen :: [Char] -> [Char]
pwdgen x =  nub ((filter isUpper x) ++ (filter (not . isUpper) x ))
main = interact pwdgen
