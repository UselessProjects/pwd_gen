<?php
$chars = [
    'upper_a' => ord('A'),
    'upper_z' => ord('Z'),
    'lower_a' => ord('a'),
    'lower_z' => ord('z'),
];

$fp = fopen('php://stdin', 'r');
while (false !== ($line = fgets($fp))) {
    $has_chars = [];
    $uppers = '';
    $lowers = '';
    for ($i = 0; $i < strlen($line); ++$i) {
        $ch = ord($line[$i]);

        if (isset($has_chars[$ch])) continue;
        $has_chars[$ch] = true;

        if ($chars['upper_a'] <= $ch && $ch <= $chars['upper_z'])
            $uppers .= $line[$i];
        if ($chars['lower_a'] <= $ch && $ch <= $chars['lower_z'])
            $lowers .= $line[$i];
    }
    echo("$uppers$lowers\n");
}
fclose($fp);
