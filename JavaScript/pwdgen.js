// 作者：gnehs, 2019.

/*
 * v3: [...new Set(toConvert.split(""))].sort((x,y)=>x.charCodeAt()>90&&y.charCodeAt()<90).join('')
 * v2: [...new Set(toConvert.split(""))].sort((x,y)=>x.charCodeAt()>90&&y.charCodeAt()<90).toString().replace(/,/g,'')
 * v1: [...new Set(toConvert.split(""))].sort(x=>x.charCodeAt()>90).sort(x=>x.charCodeAt()>90).toString().replace(/,/g,'')
 */

function converter(toConvert) {
    return [...new Set(toConvert.split(""))].sort((x,y)=>x.charCodeAt()>90&&y.charCodeAt()<90).join('')
}
