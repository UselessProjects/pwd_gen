#!/usr/bin/python3
'''
密碼產生器 OwO

輸入一個包含大小寫的句子，例如 HelloEveryOne
會將所有大寫擺於前：HEOelloveryne
再刪重複的字母（區分大小寫）：HEOelovryn

版本：v1.0.0
'''

import sys # 用於載入參數

# CONSTS
author = "@nekomata_saren (介面：pan93412)"
version = "v1.0.0"

# PRE-DEFINED VARIABLES
o_txt = "" # original_text

# USAGE TXT
usage = f"""密碼產生器 OwO
版本：{version}
作者：{author}

用法：python3 (Windows: python) {__file__} [原文] ([-h]|[--help])

[原文]: 包含大小寫的句子，例如：HelloEveryOne
       <非必須>

[-h] [--help]:  顯示說明訊息
若不指定原文，開啟程式則會從 STDIN 標準輸入讀取原文。
將會把輸出結果輸出於 STDOUT 標準輸出。
"""

# Args Determine
if "-h" in sys.argv or "--help" in sys.argv:
    print(usage)
    exit(1)
elif len(sys.argv) == 2:
    o_txt = sys.argv[1]
else:
    o_txt = input()

def converter(s_in):
  ap = set()
  s = ''
  for i in range(2):
    for c in s_in:
      if c in ap:
        continue
      cmp = c.upper() if i == 0 else c.lower()
      if cmp == c:
        s += c
        ap.add(c)
  return s

# RUN!
print(converter(o_txt))
exit()
