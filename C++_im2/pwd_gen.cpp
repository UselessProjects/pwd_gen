#include <iostream>
#include <string>
using namespace std;

int main() {
    string str;
    while (cin >> str) {
        string upper_str, lower_str;
        unsigned long has_char[4] = {};
        for (char ch : str) {
            if (has_char[ch % 4] & 1 << (ch / 4)) continue;
            has_char[ch % 4] |= 1 << (ch / 4);
            if ('a' <= ch && ch <= 'z') {
                lower_str += ch;
            } else {
                upper_str += ch;
            }
        }
        cout << upper_str << lower_str << endl;
    }
    return 0;
}
